# Artistify

<p align="center">
  <img src="./public/icon-256.png?raw=true" alt="Artistify Logo"/>
</p>

## This basic electron app displays the top artists for a spotify account, based on it's public playlists.
<br>

---

<br>

## Dev setup

1. Install dependencies

```
yarn
```

2.  Build / Watch 

```
  yarn build / yarn dev
```



