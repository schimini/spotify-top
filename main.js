const { app, BrowserWindow, ipcMain, shell } = require("electron");
const isDev = require("electron-is-dev");
const path = require("path");
const createWindow = () => {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      contextIsolation: false,
      nodeIntegration: true,
    },
    icon: isDev ? path.join(__dirname, "./public/icon.png") : undefined
  });
  win.removeMenu();
  if (isDev) {
    win.loadURL("http://localhost:3000");
  } else {
    win.loadFile(path.join(__dirname, "../build/index.html"));
  }
};

app.whenReady().then(() => {
  createWindow();

  app.on("activate", () => {
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });

  function handleNavigateCallback(event, url, win) {
    event.reply("navigate:end", url);
    win.close();
  }

  ipcMain.on("navigate:new", (event, url) => {
    const newWindow = new BrowserWindow({
      width: 800,
      height: 600,
      show: false,
      webPreferences: {
        nodeIntegration: false,
      },
      icon: isDev ? path.join(__dirname, "./public/icon.png") : undefined
    });
    newWindow.loadURL(url);
    newWindow.show();
    newWindow.webContents.on("will-navigate", function (e, newUrl) {
      handleNavigateCallback(event, newUrl, newWindow);
    });
    newWindow.webContents.on("will-redirect", function (e, newUrl) {
      handleNavigateCallback(event, newUrl, newWindow);
    });
    newWindow.webContents.on(
      "did-get-redirect-request",
      function (e, oldUrl, newUrl) {
        handleNavigateCallback(event, newUrl, newWindow);
      }
    );
    newWindow.on(
      "close",
      function () {
        authWindow = null;
      },
      false
    );
  });

  ipcMain.on("navigate:browser", (event, url) => {
    shell.openExternal(url);
  })
});

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") app.quit();
});
