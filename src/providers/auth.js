import { createContext } from "react";
import { AuthService } from "../services/auth";

export let AuthContext = createContext(AuthService);

export const AuthProvider = ({ children }) => {
  return (
    <AuthContext.Provider value={AuthService}>{children}</AuthContext.Provider>
  );
};
