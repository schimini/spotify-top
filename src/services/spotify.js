import SpotifyWebApi from "spotify-web-api-js";
const spotifyApi = new SpotifyWebApi();
export const SpotifyService = {
  setToken: (token) => {
    spotifyApi.setAccessToken(token);
  },
  getTopArtists: async function (userId, limit = 20) {
    const playlists = await spotifyApi.getUserPlaylists(userId, { limit: 50 });
    console.log(playlists);
    const tracks = await Promise.all(
      playlists.items.map(
        async (playlist) =>
          await spotifyApi.getPlaylistTracks(playlist.id, {
            limit: 50,
            fields: "items.track(id,artists,album)",
          })
      )
    );

    return Object.values(
      tracks
        .map((trackx) => trackx.items)
        .flatMap((el) => el)
        .map((track) =>
          track.track.artists.map((el) => ({ artist: el, track: track.track }))
        )
        .flat()
        .reduce((acc, el) => {
          if (!acc[el.artist.id]) {
            acc[el.artist.id] = {
              artist: el.artist,
              count: 0,
              albums: {},
              tracks: new Set(),
              unique: 0,
            };
          }
          let repeated = false;
          if (acc[el.artist.id].tracks.has(el.track.id)) {
            repeated = true;
          } else {
            acc[el.artist.id].tracks.add(el.track.id);
          }
          acc[el.artist.id].count++;
          if (!repeated) {
            acc[el.artist.id].unique++;
          }
          if (!acc[el.artist.id].albums.hasOwnProperty(el.track.album.name)) {
            acc[el.artist.id].albums[el.track.album.name] = {
              count: 0,
              unique: 0,
            };
          }
          acc[el.artist.id].albums[el.track.album.name].count++;
          if (!repeated) {
            acc[el.artist.id].albums[el.track.album.name].unique++;
          }
          return acc;
        }, {})
    )
      .sort((a, b) => b.count - a.count)
      .slice(0, limit);
  },
};
