export const AuthService = {
  isAuthenticated: false,
  token: null,
  expiration: null,
  signin(token, expires) {
    AuthService.isAuthenticated = true;
    AuthService.token = token;
    AuthService.expiration = new Date(new Date().valueOf() + expires - 60)
  },
  signout() {
    AuthService.isAuthenticated = false;
    AuthService.token = null;
    AuthService.expiration = null
  }
}