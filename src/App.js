import React, { Component, useContext } from "react";
import { Navigate, Route, Routes, useLocation } from "react-router";
import "./App.scss";
import { ArtistList as ArtistListPage } from "./pages/artist-list/ArtistList";
import { Auth as AuthPage } from "./pages/auth/Auth";
import { AuthContext, AuthProvider } from "./providers/auth";


const RequireAuth = ({ children }) => {
  let auth = useContext(AuthContext);
  let location = useLocation();

  if (!auth.isAuthenticated) {
    // Redirect them to the /login page, but save the current location they were
    // trying to go to when they were redirected. This allows us to send them
    // along to that page after they login, which is a nicer user experience
    // than dropping them off on the home page.
    return <Navigate to="/login" state={{ from: location }} />;
  }

  return children;
};

export const App = () => {
  return (
    <AuthProvider>
      <Routes>
        <Route path="/login" element={<AuthPage />} />
        <Route
          index
          path="/"
          element={
            <RequireAuth>
              <ArtistListPage />
            </RequireAuth>
          }
        />
      </Routes>
    </AuthProvider>
  );
};

export default App;
