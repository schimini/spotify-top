import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpotify } from "@fortawesome/free-brands-svg-icons";
import { useNavigate } from "react-router";
import { AuthService } from "../../services/auth";
import "./Auth.scss";
const { ipcRenderer } = window.require("electron");
function generateRandomString(length) {
  var text = "";
  var possible =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}

function parseSpotifyUrl(url, state) {
  try {
    const info = url
      .split("#", 2)[1]
      .split("&")
      .reduce((acc, item) => {
        const x = item.split("=");
        acc[x[0]] = x[1];
        return acc;
      }, {});
    if (info.state === state) {
      return info;
    } else {
      throw "Invalid state";
    }
  } catch (err) {
    console.log("Invalid URL: ", err);
    return null;
  }
}

function performLogin(navigate) {
  const client_id = process.env.REACT_APP_SPOTIFY_ID; // Your client id
  const redirect_uri = "http://localhost:3000/oauth"; // Your redirect uri

  var state = generateRandomString(16);

  //localStorage.setItem(stateKey, state);
  var scope = "playlist-read-private playlist-read-collaborative";

  var url = "https://accounts.spotify.com/authorize";
  url += "?response_type=token";
  url += "&client_id=" + encodeURIComponent(client_id);
  url += "&scope=" + encodeURIComponent(scope);
  url += "&redirect_uri=" + encodeURIComponent(redirect_uri);
  url += "&state=" + encodeURIComponent(state);

  ipcRenderer.on("navigate:end", (event, responseUrl) => {
    const tokens = parseSpotifyUrl(responseUrl, state);
    if (tokens) {
      AuthService.signin(tokens.access_token, tokens.expires_in);
      if (AuthService.isAuthenticated) {
        navigate(`/`);
      }
    }
  });
  ipcRenderer.send("navigate:new", url);
}

export const Auth = () => {
  let navigate = useNavigate();
  return (
    <button className="button" style={{margin: 'auto'}} onClick={() => performLogin(navigate)}>
      <FontAwesomeIcon icon={faSpotify} style={{marginRight: '10px'}} />
      Login with Spotify
    </button>
  );
};
