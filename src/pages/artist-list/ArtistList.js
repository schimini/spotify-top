import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useContext, useState } from "react";
import { AuthContext } from "../../providers/auth";
import { SpotifyService } from "../../services/spotify";
import ScrollArea from "react-scrollbar";
const { ipcRenderer } = window.require("electron");
import "./ArtistList.scss";

const cleanupUserUrl = (url) => url.split("?")[0].split("/").pop();
const spotifyService = SpotifyService;
const openExternalUrl = (event, url) => {
  event.preventDefault();
  ipcRenderer.send('navigate:browser', url);
}
export const ArtistList = () => {
  let auth = useContext(AuthContext);
  const [userId, setUserId] = useState("");
  const [artists, setArtists] = useState([]);
  spotifyService.setToken(auth.token);
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        width: "100%",
        padding: 10,
      }}
    >
      <div style={{ display: "flex", flexDirection: "row", width: "100%" }}>
        <input
          type="text"
          value={userId}
          style={{ flexGrow: 1, marginRight: 10 }}
          onChange={(e) => setUserId(cleanupUserUrl(e.target.value))}
          placeholder="Copy profile url"
        />
        <button
          className="button"
          onClick={() => spotifyService.getTopArtists(userId).then(setArtists)}
        >
          <FontAwesomeIcon icon={faSearch} style={{ marginRight: "10px" }} />
          Find Top Artists
        </button>
      </div>
      {artists.length > 0 && (
        <ScrollArea
          speed={0.8}
          className="area"
          style={{ marginTop: 20 }}
          contentClassName="content"
          horizontal={false}
        >
          <table id="artists">
            <thead>
              <tr>
                <th>#</th>
                <th>Artist</th>
                <th>Appearences</th>
                <th>Unique appearences</th>
              </tr>
            </thead>
            <tbody>
              {artists.map((object, i) => {
                return (
                  <tr obj={object} key={i}>
                    <td>{i + 1}</td>
                    <td>
                      <a href={object.artist.external_urls.spotify} onClick={(event) => openExternalUrl(event, object.artist.external_urls.spotify)}>
                        {object.artist.name}
                      </a>
                    </td>
                    <td>{object.count}</td>
                    <td>{object.unique}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </ScrollArea>
      )}
    </div>
  );
};
